package com.mbuguamichael;

import java.util.ArrayDeque;
import java.util.Queue;

class Node {

    int x, y, dist;

    Node(int x, int y, int dist) {
        this.x = x;
        this.y = y;
        this.dist = dist;
    }
}

class Main {
    private static final int[] row = {-1, 0, 0, 1};
    private static final int[] col = {0, -1, 1, 0};

    private static boolean isValid(int[][] mat, boolean[][] visited, int row, int col) {
        return (row >= 0) && (row < mat.length) && (col >= 0) && (col < mat[0].length)
                && mat[row][col] == 1 && !visited[row][col];
    }

    private static int findShortestPathLength(int[][] mat, Node source, Node destination) {
        if (mat == null || mat.length == 0 || mat[source.x][source.y] == 0 || mat[destination.x][destination.y] == 0) {
            return -1;
        }

        int M = mat.length;
        int N = mat[0].length;

        boolean[][] visited = new boolean[M][N];

        Queue<Node> q = new ArrayDeque<>();

        visited[source.x][source.y] = true;
        q.add(new Node(source.x, source.y, 0));

        int min_dist = Integer.MAX_VALUE;

        while (!q.isEmpty()) {
            Node node = q.poll();

            source.x = node.x;
            source.y = node.y;
            int dist = node.dist;

            if (source.x == destination.x && source.y == destination.y) {
                min_dist = dist;
                break;
            }

            for (int k = 0; k < 4; k++) {
                if (isValid(mat, visited, source.x + row[k], source.y + col[k])) {
                    visited[source.x + row[k]][source.y + col[k]] = true;
                    q.add(new Node(source.x + row[k], source.y + col[k], dist + 1));
                }
            }
        }

        if (min_dist != Integer.MAX_VALUE) {
            return min_dist;
        }
        return -1;
    }

    public static void main(String[] args) {
        int[][] mat =
                {
                        {1, 0, 0, 0},
                        {1, 1, 0, 1},
                        {0, 1, 0, 0},
                        {1, 1, 1, 1}
                };

        Node sourceNode = new Node(0, 0, 0);
        Node destinationNode = new Node(3, 3, 0);

        int min_dist = findShortestPathLength(mat, sourceNode, destinationNode);

        if (min_dist != -1) {
            System.out.println(min_dist + 1);
        } else {
            System.out.println(-1);
        }
    }
}
